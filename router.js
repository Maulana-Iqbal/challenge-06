const express = require('express')
const router = express.Router()
const userController = require('./controllers/userController');


//user_game
router.get('/user/list', userController.list)
router.post('/user/create', userController.create)
router.put('/user/update', userController.update)
router.delete('/user/destroy', userController.destroy)
//history
router.get('/history/list', userController.list)
router.post('/history/create', userController.create)
router.put('/history/update', userController.update)
router.delete('/history/destroy', userController.destroy)
//biodata
router.get('/biodata/list', userController.list)
router.post('/biodata/create', userController.create)
router.put('/biodata/update', userController.update)
router.delete('/biodata/destroy', userController.destroy)

module.exports = router