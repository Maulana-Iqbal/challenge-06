const { user_game, user_game_history,user_game_biodata } = require('../models')

module.exports = {
    list: async (req, res) => {
        try {
            const data = await user_game.findAll();

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    create: async (req, res) => {
        try {
            const data = await user_game.create({
                username: req.body.username,
                password: req.body.password,
                role: req.body.role

            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    update: async (req, res) => {
        try {
            const data = await user_game.update({
                username: req.body.username,
                password: req.body.password,
                role: req.body.role
            },{
                where : {
                    id: req.body.id
                }

            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await user_game.destroy({
                where : {
                    id: req.body.id
                }

            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    // history
    list: async (req, res) => {
        try {
            const data = await user_game_history.findAll();

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    create: async (req, res) => {
        try {
            const data = await user_game_history.create({
                point: req.body.point,
                id_user: req.body.id_user

            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    update: async (req, res) => {
        try {
            const data = await user_game_history.update({
                point: req.body.point,
                id_user: req.body.id_user
            },{
                where : {
                    id: req.body.id
                }

            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await user_game_history.destroy({
                where : {
                    id: req.body.id
                }

            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    // biodata
    list: async (req, res) => {
        try {
            const data = await user_game_biodata.findAll();

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    create: async (req, res) => {
        try {
            const data = await user_game_biodata.create({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                role: req.body.role,
                id_user: req.body.id_user


            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    update: async (req, res) => {
        try {
            const data = await user_game_biodata.update({
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                role: req.body.role,
                id_user: req.body.id_user
            },{
                where : {
                    id: req.body.id
                }

            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    },
    destroy: async (req, res) => {
        try {
            const data = await user_game_biodata.destroy({
                where : {
                    id: req.body.id
                }

            });

            return res.json({
                data: data
            })
        } catch (error) {
            return res.json({
                message : "Fatal Error!"
            })
        }
    }
} 